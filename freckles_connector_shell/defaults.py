# -*- coding: utf-8 -*-
import os

from freckles.defaults import FRECKLES_RUN_DIR, FRECKLES_CURRENT_RUN_SYMLINK
from frutils import JINJA_DELIMITER_PROFILES
from jinja2 import Environment

BASH_CONNECTOR_MODULE_FOLDER = os.path.join(os.path.dirname(__file__))
BASH_CONNECTOR_EXTERNAL_FOLDER = os.path.join(BASH_CONNECTOR_MODULE_FOLDER, "external")

DEFAULT_SHELL_CONNECTOR_FRECKLETS_FOLDER = os.path.join(
    BASH_CONNECTOR_EXTERNAL_FOLDER, "frecklets-shell"
)
FRECKLES_SHELL_CONNTECTOR_MODULE_FOLDER = os.path.dirname(__file__)
SHELL_CONNECTOR_ENVIRONMENT_TEMPLATE = os.path.join(
    BASH_CONNECTOR_EXTERNAL_FOLDER, "shell-environment-template"
)
SHELL_INTERNAL_SCRIPT_TEMPLATE_REPO = os.path.join(
    BASH_CONNECTOR_EXTERNAL_FOLDER, "script-templates"
)
SHELL_DEFAULT_REPO_ALIASES = {
    "default": {
        "frecklets": [DEFAULT_SHELL_CONNECTOR_FRECKLETS_FOLDER],
        "script-templates": [SHELL_INTERNAL_SCRIPT_TEMPLATE_REPO],
    }
}

SHELL_JINJA_ENV = Environment(**JINJA_DELIMITER_PROFILES["shell"])

SHELL_CONFIG_SCHEMA = {
    "run_folder": {
        "type": "string",
        "default": FRECKLES_RUN_DIR + "_shell",
        "__doc__": {
            "short_help": "the target for the generated shell execution bundle"
        },
    },
    "current_run_folder": {
        "type": "string",
        "default": FRECKLES_CURRENT_RUN_SYMLINK,
        "__doc__": {
            "short_help": "target of a symlink the current shell execution bundle"
        },
        "__alias__": "add_symlink_to_env",
    },
    "force_run_folder": {
        "type": "boolean",
        "default": True,
        "__alias__": "force",
        "__doc__": {
            "short_help": "overwrite a potentially already existing execution bundle"
        },
    },
    "add_timestamp_to_env": {
        "type": "boolean",
        "default": True,
        "__doc__": {
            "short_help": "whether to add a timestamp to the execution bundle folder name"
        },
    },
    "allow_remote": {
        "type": "boolean",
        "default": False,
        "__doc__": {
            "short_help": "whether to allow remote resources (binaries, script-templates, etc..."
        },
    },
    "allow_remote_script_templates": {
        "type": "boolean",
        "default": False,
        "__doc__": {
            "short_help": "whether to allow remote script templates, overwrites 'allow_remote'"
        },
    },
}

SHELL_RUN_CONFIG_SCHEMA = {
    "ssh_key": {
        "type": "string",
        "__doc__": {"short_help": "the path to a ssh key identity file"},
    },
    "connection_type": {
        "type": "string",
        "__doc__": {"short_help": "the connection type, probably 'ssh' or 'local'"},
    },
    "ssh_port": {
        "type": "integer",
        "default": 22,
        "__doc__": {
            "short_help": "the ssh port to connect to in case of a ssh connection"
        },
        "__alias__": "ansible_port",
    },
    "user": {
        "type": "string",
        "__doc__": {"short_help": "the user name to use for the connection"},
    },
    "host": {
        "type": "string",
        "__doc__": {"short_help": "the host to connect to"},
        "default": "localhost",
    },
    "host_ip": {"type": "string", "__doc__": {"short_help": "the host ip, optional"}},
    "no_run": {
        "type": "boolean",
        "coerce": bool,
        "__doc__": {
            "short_help": "only create the shell environment, don't execute anything"
        },
    },
}
