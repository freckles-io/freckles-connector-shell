# -*- coding: utf-8 -*-

"""Top-level package for freckles-connector-shell."""

__author__ = """Markus Binsteiner"""
__email__ = "markus@frkl.io"
__version__ = "0.1.0"
