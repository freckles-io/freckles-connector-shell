# -*- coding: utf-8 -*-

# python 3 compatibility
from __future__ import absolute_import, division, print_function, unicode_literals

import abc
import copy
import stat
from datetime import datetime

import logging
import os
import shutil
from collections import OrderedDict

import six
from cookiecutter.main import cookiecutter
from plumbum import local, SshMachine

from freckles.defaults import MIXED_CONTENT_TYPE
from freckles.exceptions import FrecklesConfigException
from freckles.freckles_runner import TaskDetail
from frkl import FrklistContext
from frutils import DEFAULT_EXCLUDE_DIRS, dict_merge, replace_string
from freckles.connectors import FrecklesConnector

from .defaults import (
    SHELL_CONNECTOR_ENVIRONMENT_TEMPLATE,
    SHELL_JINJA_ENV,
    SHELL_DEFAULT_REPO_ALIASES,
    SHELL_CONFIG_SCHEMA,
    SHELL_RUN_CONFIG_SCHEMA,
)

log = logging.getLogger("freckles")


# class ExecutablesIndex(LuItemIndex):
#
#     def __init__(self, base_path, require_full_paths=False):
#
#         super(ExecutablesIndex, self).__init__(url=base_path, item_type="frecklet")
#         self.require_full_paths = require_full_paths


def find_script_templates_in_repos(script_template_repos):

    if isinstance(script_template_repos, six.string_types):
        script_template_repos = [script_template_repos]

    result = {}
    for strepo in script_template_repos:
        templates = find_script_templates_in_repo(strepo)
        dict_merge(result, templates, copy_dct=False)

    return result


SCRIPT_TEMPLATE_REPO_CACHE = {}
SCRIPT_TEMPLATE_MARKERS = ["script-template"]


def find_script_templates_in_repo(script_template_repo):

    if script_template_repo in SCRIPT_TEMPLATE_REPO_CACHE.keys():
        return SCRIPT_TEMPLATE_REPO_CACHE[script_template_repo]

    result = {}
    try:
        for root, dirnames, filenames in os.walk(
            os.path.realpath(script_template_repo), topdown=True, followlinks=True
        ):
            dirnames[:] = [d for d in dirnames if d not in DEFAULT_EXCLUDE_DIRS]
            # check for extensions

            for filename in filenames:

                match = False
                st_file = os.path.join(root, filename)
                for marker in SCRIPT_TEMPLATE_MARKERS:
                    if marker in st_file:
                        match = True
                        break
                if not match:
                    continue

                result[filename] = st_file
    except (Exception) as e:
        log.warn(
            "Can't retrieve script-templates from repo '{}': {}".format(
                script_template_repo, e
            )
        )
        log.debug(e, exc_info=1)

    SCRIPT_TEMPLATE_REPO_CACHE[script_template_repo] = result
    return result


class ShellContext(FrklistContext):
    def __init__(self, allow_remote_script_templates=None, **kwargs):

        super(ShellContext, self).__init__(**kwargs)

        if allow_remote_script_templates is None:
            allow_remote_script_templates = self.allow_remote

        self.allow_remote_script_templates = allow_remote_script_templates
        self.script_template_repos = self.urls.get("script-templates")
        self.available_script_templates = find_script_templates_in_repos(
            self.script_template_repos
        )


@six.add_metaclass(abc.ABCMeta)
class ShellTaskTypeProcessor(object):
    def __init__(self, connector):

        self.connector = connector

    @abc.abstractmethod
    def process_task(self, task):
        """Takes a task description, returns a list of shell script parts incl. optional external dependencies."""

        pass


class ShellCommandProcessor(ShellTaskTypeProcessor):
    def process_task(self, task):

        # if "command" in task["task"].keys():
        #     command = task["task"]["command"]
        # else:
        #     command = task["task"].get("name")
        command = task["task"]["command"]
        name = task["task"]["name"]

        # if command is None:
        #     raise FrecklesConfigException(
        #         "Neither 'name' nor 'command' key specified in task: {}".format(
        #             task
        #         )
        #     )

        task_id = task["task"]["_task_id"]

        if "command_tokens" in task["task"].keys():
            command_tokens = task["task"]["command_tokens"]
        else:
            command_tokens = []

        vars = task.get("vars", {})
        args = []
        for token in command_tokens:
            if token not in vars.keys():
                raise Exception("Token '{}' not available in vars")
            if vars[token]:
                args.append(vars[token])

        msg = task["task"].get("__msg__", name)
        return {
            "tasks": [
                {
                    "command": command,
                    "args": args,
                    "type": task["task"]["type"],
                    "msg": msg,
                    "id": task_id,
                }
            ],
            "ext_files": {},
            "functions": {},
        }


class ShellScriptTemplateProcessor(ShellTaskTypeProcessor):
    def __init__(self, connector):

        super(ShellScriptTemplateProcessor, self).__init__(connector)

        self.command_proc = ShellCommandProcessor(connector)

    def process_task(self, task):

        template_script = task["task"]["command"]
        script_name = task["task"]["name"]

        context = self.connector.get_shell_context()

        template_script_path = context.available_script_templates.get(
            template_script, None
        )
        if template_script_path is None:
            raise FrecklesConfigException(
                "No script-template '{}' available.".format(template_script)
            )

        with open(template_script_path, "r") as ts:
            ts_content = ts.read()

        replaced_script = replace_string(
            ts_content, task.get("vars", {}), SHELL_JINJA_ENV
        )

        temp = copy.deepcopy(task)
        # that way we can use the same template multiple times if we want to
        temp["task"]["command"] = script_name
        command_desc = self.command_proc.process_task(temp)

        ext_file = {}
        ext_file["type"] = "string_content"
        ext_file["content"] = replaced_script

        # TODO: check whether it already exists?
        command_desc.setdefault("ext_files", {})[script_name] = ext_file

        return command_desc


class ShellExodusTemplateProcessor(ShellTaskTypeProcessor):
    def __init__(self, connector):

        super(ShellExodusTemplateProcessor, self).__init__(connector)

        self.command_proc = ShellCommandProcessor(connector)

    def process_task(self, task):

        binary = task["task"]["command"]

        ext_file = {}
        ext_file["type"] = "exodus-binary"
        ext_file["binary-name"] = binary

        command_desc = self.command_proc.process_task(task)

        command_desc.setdefault("ext_files", {})[binary] = ext_file

        return command_desc


class ShellConnector(FrecklesConnector):
    def __init__(self, connector_name="shell"):

        super(ShellConnector, self).__init__(connector_name=connector_name)

        self.require_absolute_path = None
        self.shell_context = None
        self.processors = {}
        self.processors["shell-command"] = ShellCommandProcessor(self)
        self.processors["script-template"] = ShellScriptTemplateProcessor(self)
        self.processors["exodus-binary"] = ShellExodusTemplateProcessor(self)

    def get_shell_context(self):

        if self.shell_context is None:
            script_template_repos = [
                r["path"]
                for r in self.content_repos
                if r["content_type"] == "script-templates"
                or r["content_type"] == MIXED_CONTENT_TYPE
            ]

            urls = {"script-templates": script_template_repos}
            allow_remote = self.get_cnf_value("allow_remote")
            allow_remote_script_templates = self.get_cnf_value(
                "allow_remote_script_templates"
            )

            if allow_remote_script_templates is None:
                allow_remote_script_templates = allow_remote

            self.shell_context = ShellContext(
                urls=urls, allow_remote_script_templates=allow_remote_script_templates
            )

        return self.shell_context

    def get_frecklet_metadata(self, name):

        return None

    def get_supported_task_types(self):

        result = ["shell-command", "script-template", "exodus-binary"]
        return result

    def get_supported_repo_content_types(self):

        return ["script-templates"]

    # def generate_shell_script(self, script_task_list, all_functions):
    #
    #     jinja_env = Environment(loader=PackageLoader("freckles", "templates"))
    #     template = jinja_env.get_template("shell_script_template.sh")
    #
    #     output_text = template.render(
    #         tasklist=script_task_list, functions=all_functions
    #     )
    #     return output_text

    def run(
        self,
        tasklist,
        context_config=None,
        run_config=None,
        result_callback=None,
        output_callback=None,
        sudo_password=None,
        parent_task=None,
    ):

        script_task_list = []
        all_ext_files = OrderedDict()
        all_functions = OrderedDict()

        for task in tasklist:

            tasktype = task["task"]["type"]
            # msg = task["task"].get("__msg__", task["task"]["name"])

            proc = self.processors.get(tasktype, None)
            if proc is None:
                raise FrecklesConfigException(
                    "No task processor for task type '{}' implemented (yet).".format(
                        tasktype
                    )
                )

            c_r = proc.process_task(task)
            tasks = c_r["tasks"]
            functions = c_r["functions"]
            ext_files = c_r["ext_files"]

            script_task_list.extend(tasks)
            for fn_name, fn_content in functions.items():

                if fn_name in all_functions.keys():
                    raise FrecklesConfigException(
                        "Duplicate function: {}".format(fn_name)
                    )
                all_functions[fn_name] = fn_content

            for f_name, f_details in ext_files.items():

                if f_name in all_ext_files.keys():
                    raise FrecklesConfigException(
                        "Duplicate external file/binary: {}".format(f_name)
                    )

                all_ext_files[f_name] = f_details

        env_dir = self.get_cnf_value("run_folder")
        force = self.get_cnf_value("force_run_folder")
        add_timestamp_to_env = self.get_cnf_value("add_timestamp_to_env")
        add_symlink_to_env = self.get_cnf_value("add_symlink_to_env")

        callback_adapter = ShellFrecklesCallbackAdapter(
            parent_task=parent_task,
            result_callback=result_callback,
            output_callback=output_callback,
        )

        runner = ShellFrecklesConnectorRunner()
        run_properties = runner.run(
            run_config=run_config,
            callback_adapter=callback_adapter,
            env_dir=env_dir,
            tasklist=script_task_list,
            ext_files=all_ext_files,
            functions=all_functions,
            force=force,
            add_timestamp_to_env=add_timestamp_to_env,
            add_symlink_to_env=add_symlink_to_env,
        )

        return run_properties

    def get_repo_aliases(self):

        return SHELL_DEFAULT_REPO_ALIASES

    def get_indexes(self):

        return []

    def get_cnf_schema(self):

        return SHELL_CONFIG_SCHEMA

    def get_run_config_schema(self):

        return SHELL_RUN_CONFIG_SCHEMA

    # def get_indexes(self):
    #
    #     return None


class ShellFrecklesConnectorRunner(object):
    def __init__(self):

        pass

    def run(
        self,
        run_config,
        callback_adapter,
        env_dir,
        tasklist,
        ext_files,
        functions,
        force,
        add_timestamp_to_env,
        add_symlink_to_env=False,
        delete_env=False,
    ):

        hostname = run_config.get_cnf_value("host")
        connection_type = run_config.get_cnf_value("connection_type")
        if connection_type == "ssh":

            remote = True
            ssh_key = run_config.get_cnf_value("ssh_key")
            user = run_config.get_cnf_value("user")
            host_ip = run_config.get_cnf_value("host_ip")
            ssh_port = run_config.get_cnf_value("ssh_port")

            # otherwise we run into problems with Vagrant
            if host_ip:
                h = host_ip
            else:
                h = hostname

            machine = SshMachine(h, port=ssh_port, user=user, keyfile=ssh_key)
        else:
            remote = False
            machine = local

        run_properties = self.render_environment(
            env_dir=env_dir,
            tasklist=tasklist,
            ext_files=ext_files,
            functions=functions,
            force=force,
            add_timestamp_to_env=add_timestamp_to_env,
            add_symlink_to_env=add_symlink_to_env,
            skip_exodus=not remote,
        )

        no_run = run_config.get_cnf_value("no_run")

        if no_run:
            return run_properties

        run_dir = run_properties["env_dir"]
        current_task = callback_adapter.parent_task
        td = TaskDetail(hostname, task_type="shell-script", task_parent=current_task)

        callback_adapter.add_command_started(td)

        if remote:
            # copy execution environment
            td = TaskDetail(
                "uploading execution environment", task_type="upload", task_parent=td
            )
            callback_adapter.add_command_started(td)
            machine.upload(run_dir, "/tmp")
            callback_adapter.add_command_result(td, rc=0, stdout=None, stderr=None)
            td = td.task_parent
            run_script = os.path.join("/tmp", run_properties["run_script_relative"])
        else:
            run_script = run_properties["run_script"]

        machine.env["ECHO_TASK_START"] = "true"
        machine.env["ECHO_TASK_FINISHED"] = "true"
        cmd = machine["bash"]

        # rc, stdout, stderr = cmd.run([run_script], retcode=None)
        current_task = td
        popen = cmd.popen(run_script)

        current_task_stdout = []
        current_task_stderr = []
        current_task_id = None
        current_msg = None

        log.debug("Reading command output...")
        for line in popen.iter_lines():

            log.debug(line)
            # print(line)
            stdout = line[0]
            if stdout:
                if stdout.startswith("STARTING_TASK["):
                    index = stdout.index("]")
                    task_id = int(stdout[14:index])
                    current_msg = stdout[index + 2 :].strip()
                    # print(stdout)
                    if task_id > current_task_id:
                        # print("starting: {}".format(msg))
                        td = TaskDetail(
                            task_name=current_msg,
                            task_type="script-command",
                            task_parent=current_task,
                            task_title=current_msg,
                            freckles_task_id=task_id,
                        )
                        callback_adapter.add_command_started(td)
                        current_task = td
                        current_task_id = task_id

                elif stdout.startswith("FINISHED_TASK["):
                    # print("finished")
                    index = stdout.index("]")
                    task_id = int(stdout[14:index])
                    rc = int(stdout[index + 2 :])
                    callback_adapter.add_command_result(
                        current_task,
                        rc=rc,
                        stdout="\n".join(current_task_stdout),
                        stderr="\n".join(current_task_stderr),
                    )
                    current_task_stdout = []
                    current_task_stderr = []
                    current_task = current_task.task_parent
                    current_msg = None
                else:
                    if stdout and current_task_id is not None:
                        current_task_stdout.append(stdout)

            stderr = line[1]
            if stderr:
                current_task_stderr.append(stderr)

        rc = popen._proc.returncode

        run_properties["return_code"] = rc
        run_properties["signal_status"] = -1

        if remote:
            td = TaskDetail(
                "deleting execution environment",
                task_type="delete",
                task_parent=current_task,
            )
            if delete_env:
                callback_adapter.add_command_started(td)
                delete = machine["rm"]
                rc, stdout, stderr = delete.run(
                    ["-r", os.path.join("/tmp", run_properties["env_dir_name"])]
                )
                callback_adapter.add_command_result(
                    td, rc, stdout=stdout, stderr=stderr
                )
            machine.close()

        return run_properties

    def render_environment(
        self,
        env_dir,
        tasklist,
        ext_files,
        functions,
        force,
        add_timestamp_to_env,
        add_symlink_to_env=False,
        skip_exodus=False,
    ):
        """Renders the bash script and the context it runs in.

        Args:
          env_dir (str): the base path to the environment
          tasklist (list): the list of tasks
          ext_files (dict): external files that are needed
          functions (dict): functions to include in the main shell script
          force (bool): whether to delete a possibly existing environment at the same location
          add_timestamp_to_env (bool): whether to append the current timestamp to the env dir name
          add_symlink_to_env (bool, str): the location of a (static) symbolic link to the latest environment (or 'False')
          skip_exodus (bool): whether to skip the creation of an exodus bundle (makes sense if executing on localhost

        Returns:
          dict: details about the rendered environment
        """

        env_dir = os.path.expanduser(env_dir)
        if add_timestamp_to_env:
            start_date = datetime.now()
            date_string = start_date.strftime("%y%m%d_%H_%M_%S")
            dirname, basename = os.path.split(env_dir)
            env_dir_name = "{}_{}".format(basename, date_string)
            env_dir = os.path.join(dirname, env_dir_name)
        else:
            env_dir_name = os.path.basename(env_dir)

        result = {}
        result["env_dir"] = env_dir
        result["env_dir_name"] = env_dir_name

        working_dir = os.path.join(env_dir, "working_dir")
        executables_dir = os.path.join(env_dir, "executables")
        run_script = os.path.join(env_dir, "run.sh")
        run_script_relative = os.path.join(env_dir_name, "run.sh")

        result["working_dir"] = working_dir
        result["executables_dir"] = executables_dir
        result["run_script"] = run_script
        result["run_script_relative"] = run_script_relative

        if os.path.exists(env_dir) and force:
            shutil.rmtree(env_dir)

        cookiecutter_details = {
            "env_dir": env_dir,
            "tasks": {"tasklist": tasklist},
            "functions": False,
        }

        log.debug("Creating shell environment from template...")
        log.debug("Using cookiecutter details: {}".format(cookiecutter_details))

        cookiecutter(
            SHELL_CONNECTOR_ENVIRONMENT_TEMPLATE,
            extra_context=cookiecutter_details,
            no_input=True,
        )

        # make run.sh executable
        st = os.stat(run_script)
        os.chmod(run_script, st.st_mode | stat.S_IEXEC)

        if add_symlink_to_env:
            link_path = os.path.expanduser(add_symlink_to_env)
            if os.path.exists(link_path):
                os.unlink(link_path)
            link_parent = os.path.abspath(os.path.join(link_path, os.pardir))
            try:
                os.makedirs(link_parent)
            except (Exception):
                pass

            os.symlink(env_dir, link_path)
            result["env_dir_link"] = link_path

        all_exodus_binaries = []
        for f_name, f_details in ext_files.items():

            target = os.path.join(executables_dir, f_name)
            f_type = f_details["type"]

            if f_type == "string_content":

                content = f_details["content"]
                with open(target, "w") as f:
                    f.write(content)

                # make executable
                st = os.stat(target)
                os.chmod(target, st.st_mode | stat.S_IEXEC)

            elif f_type == "exodus-binary":

                name = f_details["binary-name"]
                if name not in all_exodus_binaries:
                    all_exodus_binaries.append(name)

            else:
                raise FrecklesConfigException(
                    "Unknown external file type: {}".format(f_type)
                )

        if not skip_exodus:
            exodus_bundle = os.path.join(env_dir, "exodus-binaries", "bundle.sh")
            exodus_cmd = local["exodus"]
            exodus_args = ["-o", exodus_bundle]
            exodus_args.append(all_exodus_binaries)
            rc, stdout, stderr = exodus_cmd.run(exodus_args)

        result["additional_files"] = ext_files
        result["task_details"] = tasklist

        os.chmod(env_dir, 0o0700)

        return result


class ShellFrecklesCallbackAdapter(object):
    def __init__(self, parent_task, result_callback=None, output_callback=None):

        self.parent_task = parent_task
        self.result_callback = result_callback
        self.output_callback = output_callback

        self.latest_task = None

    def add_command_started(self, task_detail):

        self.output_callback.task_started(task_detail)
        self.latest_task = task_detail

    def add_command_result(self, task_details, rc, stdout, stderr):

        success = True
        changed = True
        skipped = False

        if rc != 0:
            success = False
            changed = False

        msg = ""
        if stdout:
            msg = "{}\nstdout:\n{}".format(msg, stdout)
        if stderr:
            msg = "{}\nstderr:\n{}".format(msg, stderr)

        self.output_callback.task_finished(
            task_details, success=success, msg=msg, skipped=skipped, changed=changed
        )
        self.latest_task = task_details.task_parent
