#!/usr/bin/env bash

# example template script for the freckles shell connector

VAR1="#{{:: VAR1 ::}}#"
VAR2="#{{:: VAR2 ::}}#"

echo "VAR1: ${VAR1}"
echo "VAR2: ${VAR2}"

