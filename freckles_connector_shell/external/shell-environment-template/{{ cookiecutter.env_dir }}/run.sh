#!/usr/bin/env bash

# ------------------------------------------------------------
# Bash script template for the freckles shell script connector
#
# If run outside freckles, those environment variables can be adjusted:
#
# ECHO_TASK_START=false
# ECHO_TASK_FINISH=false
#
#
# Copyright 2018 by Markus Binsteiner
# licensed under the Parity Public License 3.0.0
# ------------------------------------------------------------


# ----------------------------------------------------
# freckly init stuff

THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -e "$HOME/.nix-profile/etc/profile.d/nix.sh" ]; then source "$HOME/.nix-profile/etc/profile.d/nix.sh"; fi

if [ -d "$HOME/.local/share/inaugurate/conda/envs/inaugurate/bin" ]; then
    export PATH="$PATH:$HOME/.local/share/inaugurate/conda/envs/inaugurate/bin"
fi
if [ -d "$HOME/.local/share/inaugurate/conda/bin" ]; then
    export PATH="$PATH:$HOME/.local/share/inaugurate/conda/bin"
fi
if [ -d "$HOME/.local/share/inaugurate/bin" ]; then
    export PATH="$PATH:$HOME/.local/share/inaugurate/bin"
fi
if [ -d "$HOME/.local/bin" ]; then
    export PATH="$PATH:$HOME/.local/bin"
fi

if [ -f "$THIS_DIR/exodus-binaries/bundle.sh" ]; then
    $THIS_DIR/exodus-binaries/bundle.sh $THIS_DIR/exodus-binaries
    export PATH="$THIS_DIR/exodus-binaries/bin:$PATH"
fi

export PATH="$THIS_DIR/executables:$PATH"

if [ -d "${THIS_DIR}/working_dir" ]; then
    cd "${THIS_DIR}/working_dir"
fi

# ----------------------------------------------------

function start_task
{

    if [ "$ECHO_TASK_START" = true ]; then
        echo "STARTING_TASK[${1}]: ${2}"
    fi

}

function task_finished
{

    if [ "$ECHO_TASK_FINISHED" = true ]; then
        echo "FINISHED_TASK[${1}]: ${2}"
    fi

}

{% if cookiecutter.functions %}
# ----------------------------------------------------
# functions

# template

# ----------------------------------------------------
{% endif %}

# start script
# ----------------------------------------------------

{% for task in cookiecutter.tasks.tasklist %}
# {{ task.msg }}
start_task "{{ task.id }}" "{{ task.msg }}"
{{ task.command }} {{ task.args | join(' ') }}
task_finished "{{ task.id }}" "$?"

{% endfor %}
