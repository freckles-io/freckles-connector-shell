=======================
freckles-connector-shell
=======================


.. image:: https://img.shields.io/pypi/v/freckles_connector_shell.svg
           :target: https://pypi.python.org/pypi/freckles_connector_shell
           :alt: pypi

.. image:: https://readthedocs.org/projects/freckles-connector-shell/badge/?version=latest
           :target: https://freckles-connector-shell.readthedocs.io/en/latest/?badge=latest
           :alt: Documentation Status

.. image:: https://gitlab.com/freckles-io/freckles-connector-shell/badges/develop/pipeline.svg
           :target: https://gitlab.com/makkus/freckles_connector_shell/pipelines
           :alt: pipeline status


.. image:: https://pyup.io/repos/github/makkus/freckles_connector_shell/shield.svg
           :target: https://pyup.io/repos/github/makkus/freckles_connector_shell/
           :alt: Updates

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
           :target: https://github.com/ambv/black
           :alt: codestyle



Freckles connector that creates bash scripts.

* License: Parity Public License 3.0.0
* Documentation: https://freckles-connector-shell.readthedocs.io.


Features
--------

* TODO
