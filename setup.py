#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    "freckles==0.6.0",
    "exodus-bundler==2.0.2",
    "cookiecutter==1.6.0",
]

setup_requirements = ['pytest-runner', ]

test_requirements = ['pytest', ]

setup(
    author="Markus Binsteiner",
    author_email='markus@frkl.io',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: Other/Proprietary License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    description="Freckles connector that creates shell scripts.",
    entry_points={
        'freckles.connectors': [
            "shell=freckles_connector_shell.freckles_connector_shell:ShellConnector"
        ],
        # 'freckfreckfreck.plugins': [
        #     "nsbl=freckles_connector_nsbl.fff_plugin_nsbl:nsbl",
        #     "last-run-nsbl=freckles_connector_nsbl.fff_plugin_last_run_nsbl:last",
        # ]
    },
    install_requires=requirements,
    license="Parity Public License 5.0.0",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='freckles_connector_shell',
    name='freckles-connector-shell',
    packages=find_packages(include=['freckles_connector_shell']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/freckles-io/freckles-connector-shell',
    version='0.1.0',
    zip_safe=False,
)
